import React, { Component } from 'react';
import { AppRegistry, Image, StyleSheet, Text, ScrollView } from 'react-native';

const styles = StyleSheet.create({
  bigBlue: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 96,
  },

  bigRed: {
    color: 'red',
    fontWeight: 'bold',
    fontSize: 96,
  },
});
export default class Bananas extends Component {
  render() {
    let pic = {
      uri: 'https://thumbs.gfycat.com/FaithfulSnivelingIchidna-max-1mb.gif'
    };
    return (
      <ScrollView>
       <Image source={pic} style={{width: 400, height: 200}}/>
       <Text style={styles.bigRed}>Ozymandias</Text>
       <Text style={styles.bigBlue}>quatro</Text>
       <Text style={[styles.bigBlue, styles.bigRed]}>tt, tt</Text>
       <Text style={[styles.bigRed, styles.bigBlue]}>ab, cd</Text>
       <Text style={{fontSize:96}}>Juliano é muito gato meu deus do céu</Text>
      </ScrollView>
    );
  }
}
